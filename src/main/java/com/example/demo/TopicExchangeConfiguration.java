package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Slf4j
@Configuration
@Profile("topic")
public class TopicExchangeConfiguration {

    @Bean
    Queue redQueue() {
        return new Queue("redQueue");
    }

    @Bean
    Queue blueQueue() {
        return new Queue("blueQueue");
    }

    @Bean
    Queue colorQueue() {
        return new Queue("colorQueue");
    }

    @Bean
    Queue myQueue() {
        return new Queue("myQueue");
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange("topic-exchange");
    }

    @Bean
    Binding redBinding(Queue redQueue, TopicExchange exchange) {
        return BindingBuilder.bind(redQueue).to(exchange).with("color.red");
    }

    @Bean
    Binding blueBinding(Queue blueQueue, TopicExchange exchange) {
        return BindingBuilder.bind(blueQueue).to(exchange).with("color.blue");
    }

    @Bean
    Binding colorBinding(Queue colorQueue, TopicExchange exchange) {
        return BindingBuilder.bind(colorQueue).to(exchange).with("color.*");
    }

    @Bean
    Binding myBinding(Queue myQueue, TopicExchange exchange) {
        return BindingBuilder.bind(myQueue).to(exchange).with("queue.my");
    }

    @RabbitListener(queues = {"colorQueue"})
    public void listen(String in) {
        log.info("Received message from the colored queue: {}", in);
    }

    @RabbitListener(queues = {"redQueue"})
    public void listenRed(String in) {
        log.info("Received message from red queue: {}", in);
    }

    @RabbitListener(queues = {"myQueue"})
    public void listenMy(String in) {
        log.info("Received message from my queue: {}", in);
    }
}
