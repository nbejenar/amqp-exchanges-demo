package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.HeadersExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Slf4j
@Configuration
@Profile("headers")
public class HeaderExchangeConfiguration {

    @Bean
    Queue redQueue() {
        return new Queue("redQueue");
    }

    @Bean
    Queue blueQueue() {
        return new Queue("blueQueue");
    }

    @Bean
    HeadersExchange exchange() {
        return new HeadersExchange("headers-exchange");
    }

    @Bean
    Binding redBinding(Queue redQueue, HeadersExchange exchange) {
        return BindingBuilder.bind(redQueue).to(exchange).where("color").matches("red");
    }

    @Bean
    Binding blueBinding(Queue blueQueue, HeadersExchange exchange) {
        return BindingBuilder.bind(blueQueue).to(exchange).where("color").matches("blue");
    }

    @RabbitListener(queues = {"redQueue"})
    public void listenRed(String in) {
        log.info("Received message from red queue: {}", in);
    }

    @RabbitListener(queues = {"blueQueue"})
    public void listenBlue(String in) {
        log.info("Received message from blue queue: {}", in);
    }
}
